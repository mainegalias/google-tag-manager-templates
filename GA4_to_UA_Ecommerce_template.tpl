﻿___TERMS_OF_SERVICE___

By creating or modifying this file you agree to Google Tag Manager's Community
Template Gallery Developer Terms of Service available at
https://developers.google.com/tag-manager/gallery-tos (or such other URL as
Google may provide), as modified from time to time.


___INFO___

{
  "type": "TAG",
  "id": "cvt_temp_public_id",
  "version": 1,
  "securityGroups": [],
  "displayName": "GA4 to UA Ecommerce",
  "brand": {
    "id": "brand_dummy",
    "displayName": "",
    "thumbnail": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAcHBwcHBwgJCQgLDAsMCxAPDg4PEBkSExITEhklFxsXFxsXJSEoIR4hKCE7LykpLztFOjc6RVNKSlNpY2mJibgBDQ0NDQ4NDxAQDxUWFBYVHhwZGRweLiEjISMhLkYrMysrMytGPks9OT1LPm9XTU1Xb4BrZmuAm4uLm8O5w//////CABEIAKoAqgMBEQACEQEDEQH/xAA0AAEAAgMBAQEAAAAAAAAAAAAAAQQFBgcDAggBAQADAQEBAAAAAAAAAAAAAAADBAUCAQb/2gAMAwEAAhADEAAAAP0iAAAAAAAAAAAAAAAAVu47PEgAAAAEAAAAEGq2qG11b4AAAkAgAAAA+XnPtDI6Hn7AAAAkAgAAHyD6KfcfONHE6nl78HyfYABIBAAAPh5rdins1e7hpq3OdLE6/j/SePvOCmq7JBcAAkAgAAEHPr+PsVe7X6451pYna8T6nlupgdGztq9xKABIBABBR7jo9w5uG1jJIOP7PzW5UdTSL+Tvufr4GxU6zk/ReHvGFmrbBBb+3okAgAEGrWaGhaGRtlTQ027mY2WvAN5oa9XuOp1x1HL3bXMkgkAgAAgws1bk+v8AOUu4gANwpafS8zd9HsgEgHl75h5a+HmrUJYKPcNCSKj3DRki8/eQHoW45L3E16OW9HNd4mykU+z17npz2IB8e+U+o6EkNHuKjJDpV3Lx0sAD08bfT089Xt3eJb8c17iWxz19PQIABB8vOc6OLpl7L9fPa3fAeLnEo6vkfRZuG19AEgEAEHh7zoV7Juc97ZU0eLbfy3vz3j5Yb8cu40tTYq93UrWdsla9sle79AkAgAFf3n386Gr2aGn3M3aKt/nWli9dx/o+YauD2bF+nk8fefTzr7BIBAAAIOe38fe6OrhZ63OdHE7NjfT6paz73EufgtgASAQAADzeUu48hxLjJIOcaWJ1rJ+iGN7hyfEwAEgEAAAEEnh1xzjRxunZm4IJAAJAIAAAAINGvZe9UdQAAASAQAAAADBz1c5BaAAAEgAAAAAAAAAAAAAAAAAAAAAA/8QALBAAAQQCAQQCAAUFAQAAAAAAAwECBAUAEQYSEyAwEDIUFSExMyIkQENEUP/aAAgBAQABCQD/AB95vN5vN5vN5vN4rlyLYvlPmN1vN5vN5vN5vN+7jcjvHuk9Kex69LXOzhZuqVYt36E9LnI1FVV38WBO1Alvzhb+i1ez56kRURV8U9L2o5rmuSosVHLPVSXZyEnbpZ65xciDvInwc4owSFK+kkltCybB6eKelc5jFeA8SwCtDcsto39S8uf0UpUymJ2raA7F0mcjunWklsOKtfEbBhx47fJPJcmlmAZ3Y4YXIqyavR3t7y5g/mFbJAiQZp66UOQFeS2YLGlhFCsd/bkAfnKr3oR8CM/ikH8XasIqZIlR4rOs5w3yTzqGuAzq0iOX4TzXL3jgbNFMDAWVxSmcFCweahfpswFw2Mlgd8UnUvTreKqqqqq0NpWUlcrzEncxnn22Myrp518fvHLEhx4IGAAPXynpt6WLbi0/J9dKrZChkD8aHjBJvTJmNGNgmNGxvgnyQiDarlSRfRo/2jm5rDZ9YROckX+KA/mlsv0GTlF2T/qJcWpt9c8hSlXZCeI5s0Ou3KHyC6F+jZ4+XXQ/3IPm89v8kUXORrruQQcshH0iRK2WKYj3MZ4OGN/2YSqrC764JeNUhN/2ROHVD/pheDxv9U6zhAgSFAOX41fGY1mBDMsh8Jrk+8kfE6RmthFQ0wvrAHDiC+kZERP2xPQ5zWIrnLf8pcfrjQXiEUz0YIcqJIhF7Uhn6/Lq6akYcrsQZ8muO08clPeRrcX6eKeciSCIF5jEnmuuSO7USPB4XGHp0w4Y0KuA7tBnSnTZkiQ7DVsiPAjzCYidTmpllXHrJTgGThU3bZMJyz+PVU/bngk8TsoJUPAPUXjjuSJPHv5TzLFAZ7HlFr45ZO/C1bhtXjdGtkbvmbzISLUiciQB92dEZl1UitoijXK8xqa4Epmou81ho4JDOgomsRjUanynpXLSObkF9+FEsaOGIAYRM5SPro5WUA+7dQG/HJaL8xCsgDeOTVm1YFcvinpKr0Y9WJXQBwAdCLl0Pu1M9mcSH13QVzXwCCkWcY4fJPZJZ3Ix2ZwkarYSH/OvJPYucQCopNv6U9i7ymhkiltVeL/1v//EADERAAIBAwIDBgYCAgMAAAAAAAECAwAEERIxBSFREDBAQWFxEyAiMmKBQ5FCUBQjc//aAAgBAwEBPwD/AEMb6y/4tjw1k+prj/0PhScAmuGNmSf15+FlOmKQ9FNcLOLhh1XwR5ioJ8SPBJ9y7eo7LxtNtKfxrh5xdJ+x2OyopZjgCrZ2mLynkNlHp4DiSFGjnXfYmrS5FxH0cciK4icWrepFWrYuIz+XZe3RncRR/aDj3qGMRRIg8h3sjOoyihuoqK8gk5atLdG5dlzF8WB0qGV4JAy7g1fTpNaxsh5FqiOJEPRhV/d4BijPM7muHRfEuAx2QZ7HkRBl2A96W6EraYVLdW2FDu7uyWf6l5PSz3Ns2jJGPI1DxRDylXT6iroJ8ZihyrcxWTjHYTkkmrSeC2gyzZdueBUvE5X5RgIKt7aa6bW7HT1NRxpGgRBgDvbm1jnXnyPkamgkhfS4/fX5rOwMmJJeS+QoAAAAYA7gnAp7pE3Vz7Kabica/wAT03FT5RUeKTnZVFHiF0f88ewprq4beVqZ3b7mJ9z8yzSrtIw/dC9uR/KaXiVyPNT7il4rJ5xqaXiq+cR/RpL+Nv45P6pJA4yAw9xj5SoO4FGCFt41/qmsrVt4xR4bbHbUKbhUflIwq4iSF9KyauvzW9hHOgYTe4A2ocLhG7MaHDrUf4Z/dC0txtEtCKNdkUfqsDuSQBk8hV5fk5ji282pVZyAoJqSJ4m0uMH5DBNoD6cqfMc6imkhfUh96trpLheXJvMd47qilmIAFTNdXZ0xoVj6nlmoeFxrzkYselKkUK/SoUCp5DLK7nzNPbukKyty1NyFAZIFTwPA+lvcGuFy/S8R9xU1nBLumD1FPw6eJtcL5q3uix+HMuiT12PdlVbcZ7eIS/DtyPN+VWNoZn1sPoB/uuJKP+OPRhUAzNGPyFXNus8ek7jY1AWtrkauWDg9rKrDBAPfXCNd3YjH2IOZpEVFCKMACr8ZtXqyGbmL37L60+MutPvAqyl+JAud15H9d+c4ON6iiES4G5OSep7LkZt5R+JrhozcqeintWIJKzLs249fBSDKOOoNcLX/ALpD0Xw3Dk0yXB/LHhraMo0+RjMhP+3/AP/EAC0RAAIBAwIDBwQDAQAAAAAAAAECAwAEETFBEiFhECAwQEJRcRMiIzJQcpGB/9oACAECAQE/AP4Fl4QvUZ8tcLhYf6eVFXo+yPysYy6jqKvR+IdD5OWPKLKuh16HstxmZPmrsZgbsALEAa1MgiCx76t5CzYMHjPzU8JibodKtBmYdAanGYn+Oy2hEa8bakf5Uj8bs3ufFQKTgnHWnt5U54yPcdkL/TkVutSIsqlTVtGUncHYU4yjDoatYM/kbTaruThixu3YqsxwoJpoCi8Uhx7DfxILlozg81pooZgGwDncVJZMOaMDUHF9MBhgjke5PFLNLgDAG9R2SD9jxVLNHAOFAM+wpnZzxMcnxYZniPLTcVHIkqgqe9cXQX7U/bc0SScnwAM0sDN6k/2hZufWtCxG70LKLctQtYB6c0IIhogoKq6ADvGOM6oKNvCfQKNnCfcUbFNnNGxOzimtXX1p/tMhXUj/AIe7kjehLINHNC5mHroXk3Q0L590FRO0i8RTh70t08bcJjo3smygUbuY+oCjPMdXNF2OrHw7e1x90muwosqjJOB1pHVxlT3Pqx8RXiAI1Bp41kXDCpoGiPuux8RVLHAFRiC3GXbL9KkvWPJBiizyHmSTUacCKvsKWZWkZBsKJwCailWVcir1OayD4NR3EqaHPQ0t3FIOGRalhC/dGQy+GCRoe20TjlB2XnVzP9NeEH7jVmfynqKkOI3PQ1DKYmzsdRUgE0JxuMjtBI0PjRMIIOI/s2lMxYljqatTiZauDiF/jstpzG3Cf1NXCcEp9jzHkHcuc7AYA7ITiVPmrw4hPUjtLlkAOq6eSU4YHrV6fxoPc+Wu2ysXx5aZwwi56J/L/wD/xAA7EAACAQIACgYHCAMBAAAAAAABAgMAEQQQEiEiMTJBUZETMEBhcXIgIzNDUqGxJEJQYoGCktFEosHC/9oACAEBAAo/APwFQIsIaMW4AA9m/wA0t2XUCeVbah/9uxWA1nHswOflW3g7fKxx5zqHX3BBBHca9bCSImPvI9Y/UDFriyeZtWZsteYxBURbsTRWMnooF4INZ8SewFW2Cw3MM6mgJ47CRf8A0K25Y1r36DnmxXhVwBb3j17NAD3neetWYAaUd7MfKeNdFLqMcugb4tIrlJ5lzirOhzjiN6ms0k+ku9Sq6jWzIh5GtI+2Ybh8FaGDr0h8dS4kiXixtTTW25n0Y1/6aubZza3ViPCfk/mp0KGxifSXkaMZ+OPSXlSvDIekQruytYo2ve2IknOSaLzzNlGOMXIAzAGlgXjtPUnQg6Urm5Pct6CIu4fU9bkyqNCQDOO48RRU6wdzDiD6RSDWqai/9CgqqLAAWAHUMfKCTyFYYfCBqwg+ay0g8zk1g6fsLVk+VAKnN/zkU7+ZifSmXwc1KfMQ31qJ/NGKgbwJWm/bIKwwHujy/pUy5NvaRlNfC/oq3iAagb9gpV8pIqdPB71KPMgNLOV2iq2Cnh6QPxKI86ngbmsIfktSOfzSGof1F6hXwQCrdSAALkmiI9Ty6i3ctM7HUqi5rIkyQSpOcA8fQcwvqdRcZuNtVZLD9Qw4EVkTqNOM/UcR1ioii5Jp4sD+J9AP3mmlPwJorUcUaqS2SLZhxNe0ckdw3CrLM5CLvsBe9ayBXerbmXiK1esTwOY0I3P349E0JChuv3HFGDCxqDDJEnh1auU2crOB3gccenOejHhravs0bfzb4a2J15EEVtToPnQEi5434H+jRQxyZEg7jmONHXgwvRsBvN+tIhwZbSPw3tQVEWwFbJRuRrVKDyF8X2iNf5rwrTi9U/ivXgtknJHfWU7sXlfe7nWcXuGPLPWzHI2MCPCBeRODr94eO/sW1E45itiC3M9m2ZRHyJ7My9JhrMtxa62/F//Z"
  },
  "description": "From GA4 to UA Ecommerce event converter. This template automatically converts GA4 Ecommerce events to UA Enhanced Ecommerce events.",
  "containerContexts": [
    "WEB"
  ]
}


___TEMPLATE_PARAMETERS___

[
  {
    "type": "SELECT",
    "name": "ecommEvt",
    "displayName": "Ecommerce Event",
    "macrosInSelect": false,
    "selectItems": [
      {
        "value": "purchase",
        "displayValue": "Purchase: gaPurchase"
      },
      {
        "value": "addToCart",
        "displayValue": "Cart Addition: gaAddToCart"
      },
      {
        "value": "productImpression",
        "displayValue": "Product Impression: gaProductImpression"
      },
      {
        "value": "productClick",
        "displayValue": "Product Click: gaProductClick"
      },
      {
        "value": "productDetails",
        "displayValue": "Product Detail: gaProductDetails"
      },
      {
        "value": "removeFromCart",
        "displayValue": "Cart Removal: gaRemoveCart"
      },
      {
        "value": "checkout",
        "displayValue": "Checkout Step: gaCheckoutStepN"
      },
      {
        "value": "checkoutOption",
        "displayValue": "Checkout Step Option: gaCheckoutOption"
      },
      {
        "value": "promotionImpression",
        "displayValue": "Promotion Impression: gaPromotionImpression"
      },
      {
        "value": "promotionClick",
        "displayValue": "Promotion Click: gaPromotionClick"
      }
    ],
    "simpleValueType": true
  },
  {
    "type": "TEXT",
    "name": "checkoutStepNo",
    "displayName": "Checkout Step Number",
    "simpleValueType": true,
    "enablingConditions": [
      {
        "paramName": "ecommEvt",
        "paramValue": "checkoutOption",
        "type": "EQUALS"
      }
    ],
    "valueValidators": [
      {
        "type": "NON_EMPTY"
      }
    ]
  },
  {
    "type": "TEXT",
    "name": "checkStepTotal",
    "displayName": "Number of checkout steps in the funnel",
    "simpleValueType": true,
    "valueValidators": [
      {
        "type": "NON_EMPTY"
      }
    ],
    "enablingConditions": [
      {
        "paramName": "ecommEvt",
        "paramValue": "checkout",
        "type": "EQUALS"
      }
    ]
  }
]


___SANDBOXED_JS_FOR_WEB_TEMPLATE___

const log = require('logToConsole');

const queryPermission = require('queryPermission');
const createQueue = require('createQueue');
const dataLayerPush = createQueue('dataLayer');
const copyFromDataLayer = require('copyFromDataLayer');

const selectedEcommEvent = data.ecommEvt;
const clearDatalayer = {
    'ecommerce': null
};

let i, eventName, eventDetails, ecommerce, ecomm, evt, actionField, itemCategory;
let products = [];
let promotions = [];

if (queryPermission('read_data_layer', 'ecommerce')) {
    ecommerce = copyFromDataLayer('ecommerce');
}

if (ecommerce.items) {

    for (i = 0; i < ecommerce.items.length; i++) {
        const item = ecommerce.items[i];
        if (item.item_category) {
            itemCategory = item.item_category;
        }
        if (item.item_category2) {
            itemCategory += "/" + item.item_category2;
        }
        if (item.item_category3) {
            itemCategory += "/" + item.item_category3;
        }
        if (item.item_category4) {
            itemCategory += "/" + item.item_category4;
        }
        if (item.item_category5) {
            itemCategory += "/" + item.item_category5;
        }

        //Build Product Field Object
        products[i] = {
            'name': item.item_name ? item.item_name : undefined,
            'id': item.item_id ? item.item_id : undefined,
            'price': item.price ? item.price : undefined,
            'brand': item.item_brand ? item.item_brand : undefined,
            'category': itemCategory,
            'variant': item.item_variant ? item.item_variant : undefined,
            'quantity': item.quantity ? item.quantity : 0,
            'coupon': item.coupon ? item.coupon : undefined
        };

        //Build promotion object
        promotions[i] = {
            'id': item.promotion_id ? item.promotion_id : undefined,
            'name': item.promotion_name ? item.promotion_name : undefined,
            'creative': item.creative_name ? item.creative_name : undefined,
            'position': item.creative_slot ? item.creative_slot : undefined
        };
    }
}

if (selectedEcommEvent === "promotionImpression") {
    eventName = "gaPromotionImpression";
    ecomm = {
        'promoView': promotions
    };
}

if (selectedEcommEvent === "promotionClick") {
    eventName = "gaPromotionClick";
    ecomm = {
        'promotionClick': promotions
    };
}

if (selectedEcommEvent === "productImpression") {
    eventName = "gaProductImpression";

    for (i = 0; i < ecommerce.items.length; i++) {
        const item = ecommerce.items[i];
        products[i].list = item.item_list_name;
        products[i].position = item.index;
    }

    ecomm = {
        'currencyCode': ecommerce.currency ? ecommerce.currency : ecommerce.items[0].currency,
        'impression': products
    };
}

if (selectedEcommEvent === "productClick") {
    eventName = "gaProductClick";

    for (i = 0; i < ecommerce.items.length; i++) {
        const item = ecommerce.items[i];
        products[i].position = item.index;
    }

    actionField = {
        'list': ecommerce.items[0].item_list_name
    };

    ecomm = {
        'click': {
            'actionField': actionField,
            'products': products
        }
    };
}

if (selectedEcommEvent === "productDetail") {
    eventName = "gaProductDetail";

    actionField = {
        'list': ecommerce.items[0].item_list_name
    };

    ecomm = {
        'detail': {
            'actionField': actionField,
            'products': products
        }
    };
}

if (selectedEcommEvent === "addToCart") {
    eventName = "gaAddToCart";
    ecomm = {
        'currencyCode': ecommerce.currency ? ecommerce.currency : ecommerce.items[0].currency,
        'add': {
            'products': products
        }
    };
}

if (selectedEcommEvent === "removeFromCart") {
    eventName = "gaRemoveFromCart";
    ecomm = {
        'remove': {
            'products': products
        }
    };
}

if (selectedEcommEvent === "checkout") {
    let totalStep = data.checkStepTotal;
    let x = 0;

    while (totalStep > 0) {
        x++;
        eventName = "gaCheckoutStep" + x;
        actionField = {
            'step': x
        };

        ecomm = {
            'checkout': {
                'actionField': actionField,
                'products': products
            }
        };

        let checkoutEvent = {
            'event': eventName,
            'ecommerce': ecomm
        };

        dataLayerPush(clearDatalayer);
        dataLayerPush(checkoutEvent);

        totalStep--;
    }

}

if (selectedEcommEvent === "checkoutOption") {
    eventName = "gaCheckoutOption";
    actionField = {
        'step': data.checkoutStepNo,
        'option': ecommerce.payment_type ? ecommerce.payment_type : ecommerce.shipping_tier,
    };

    ecomm = {
        'checkout_option': {
            'actionField': actionField,
            'products': products
        }
    };
}


if (selectedEcommEvent === "purchase") {
    eventName = "gaPurchase";
    actionField = {
        'id': ecommerce.transaction_id,
        'affiliation': ecommerce.affiliation,
        'revenue': ecommerce.value,
        'tax': ecommerce.tax,
        'shipping': ecommerce.shipping,
        'coupon': ecommerce.coupon
    };

    ecomm = {
        'purchase': {
            'actionField': actionField,
            'products': products
        }
    };
}

if (selectedEcommEvent !== "checkout") {
    let event = {
        'event': eventName,
        'ecommerce': ecomm
    };

    if (queryPermission('access_globals', 'readwrite', 'dataLayer')) {
        dataLayerPush(clearDatalayer);
        dataLayerPush(event);
    }
}

data.gtmOnSuccess();


___WEB_PERMISSIONS___

[
  {
    "instance": {
      "key": {
        "publicId": "logging",
        "versionId": "1"
      },
      "param": [
        {
          "key": "environments",
          "value": {
            "type": 1,
            "string": "all"
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "access_globals",
        "versionId": "1"
      },
      "param": [
        {
          "key": "keys",
          "value": {
            "type": 2,
            "listItem": [
              {
                "type": 3,
                "mapKey": [
                  {
                    "type": 1,
                    "string": "key"
                  },
                  {
                    "type": 1,
                    "string": "read"
                  },
                  {
                    "type": 1,
                    "string": "write"
                  },
                  {
                    "type": 1,
                    "string": "execute"
                  }
                ],
                "mapValue": [
                  {
                    "type": 1,
                    "string": "dataLayer"
                  },
                  {
                    "type": 8,
                    "boolean": true
                  },
                  {
                    "type": 8,
                    "boolean": true
                  },
                  {
                    "type": 8,
                    "boolean": true
                  }
                ]
              }
            ]
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "read_data_layer",
        "versionId": "1"
      },
      "param": [
        {
          "key": "keyPatterns",
          "value": {
            "type": 2,
            "listItem": [
              {
                "type": 1,
                "string": "ecommerce.*"
              }
            ]
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  }
]


___TESTS___

scenarios:
- name: Purchase
  code: |-
    const mockData = {
      ecommEvt: "purchase",
      event: "purchase",
      ecommerce: {
          transaction_id: "T12345",
          affiliation: "Google Merchandise Store",
          value: 36.32,
          tax: 4.90,
          shipping: 5.99,
          currency: "USD",
          coupon: "SUMMER_SALE",
          items: [
           {
            item_id: "SKU_12345",
            item_name: "Stan and Friends Tee",
            affiliation: "Google Merchandise Store",
            coupon: "SUMMER_FUN",
            currency: "USD",
            discount: 2.22,
            index: 0,
            item_brand: "Google",
            item_category: "Apparel",
            item_category2: "Adult",
            item_category3: "Shirts",
            item_category4: "Crew",
            item_category5: "Short sleeve",
            item_list_id: "related_products",
            item_list_name: "Related Products",
            item_variant: "green",
            location_id: "L_12345",
            price: 9.99,
            quantity: 1
          },
          {
            item_id: "SKU_12346",
            item_name: "Google Grey Women's Tee",
            affiliation: "Google Merchandise Store",
            coupon: "SUMMER_FUN",
            currency: "USD",
            discount: 3.33,
            index: 1,
            item_brand: "Google",
            item_category: "Apparel",
            item_category2: "Adult",
            item_category3: "Shirts",
            item_category4: "Crew",
            item_category5: "Short sleeve",
            item_list_id: "related_products",
            item_list_name: "Related Products",
            item_variant: "gray",
            location_id: "L_12345",
            price: 20.99,
            promotion_id: "P_12345",
            promotion_name: "Summer Sale",
            quantity: 1
          }]
      }
    };

    // Call runCode to run the template's code.
    runCode(mockData);

    // Verify that the tag finished successfully.
    assertApi('gtmOnSuccess').wasCalled();
setup: ''


___NOTES___

Created on 1/25/2022, 5:01:44 PM


